package keycloak

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/proskive/microservices/user-service/config"
	"gopkg.in/resty.v1"
	"strings"
	"time"
)

var serviceToken string
var obtained time.Time
var lifetime time.Duration

func GetRestToken() (string, error) {
	// Is a token already available and valid?
	if serviceToken != "" {
		now := time.Now()
		if obtained.Add(lifetime).After(now) {
			return serviceToken, nil
		}
	}

	// Check if all settings are set
	if config.Get().Keycloak == nil ||
		config.Get().Keycloak.Client == "" ||
		config.Get().Keycloak.Secret == "" {
		return "", errors.New("missing settings for Keycloak adapter")
	}

	// Set obtained time
	obtained = time.Now()

	// Make request to obtain token
	resp, err := resty.R().
		SetBasicAuth(config.Get().Keycloak.Client, config.Get().Keycloak.Secret).
		SetFormData(map[string]string{
			"grant_type": "client_credentials",
		}).
		Post(tokenUrl())

	if err != nil {
		return "", err
	}

	// Unmarshal response
	tokenResponse := &TokenResource{}
	json.Unmarshal(resp.Body(), tokenResponse)

	// Get lifetime and save token
	lifetime = time.Duration(tokenResponse.ExpiresIn) * time.Second
	serviceToken = tokenResponse.AccessToken

	return serviceToken, nil
}

func RestUrl(path string) (string, error) {
	if config.Get().Keycloak == nil || config.Get().Keycloak.Url == "" || config.Get().Keycloak.Realm == "" {
		return "", errors.New("missing settings for Keycloak adapter")
	}

	if !strings.HasPrefix(path, "/") {
		path = fmt.Sprintf("/%v", strings.Trim(path, " "))
	}

	return fmt.Sprintf("%v/auth/admin/realms/%v%v", config.Get().Keycloak.Url, config.Get().Keycloak.Realm, path), nil
}

func tokenUrl() string {
	return fmt.Sprintf("%v/auth/realms/%v/protocol/openid-connect/token", config.Get().Keycloak.Url, config.Get().Keycloak.Realm)
}
