package keycloak

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/proskive/microservices/user-service/config"
	"net/http"
)

func LogRequestError(msg string, r *http.Request, err error) {
	log.Error().
		Str("Request URI", r.RequestURI).
		Str("Keycloak URL", config.Get().Keycloak.Url).
		Str("Realm", config.Get().Keycloak.Realm).
		Str("Client", config.Get().Keycloak.Client).
		Err(err).Msg(msg)
}