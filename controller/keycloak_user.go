package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"
	"gitlab.com/proskive/microservices/user-service/keycloak"
	"gitlab.com/proskive/microservices/user-service/proskive"
	"gopkg.in/resty.v1"
	"io/ioutil"
	"net/http"
)

func (k *KeycloakController) GetAllUsers(w http.ResponseWriter, r *http.Request)  {
	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/users")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do the request
	resp, err := resty.R().
		SetAuthToken(token).
		Get(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Convert response
	var keycloakUsers []keycloak.UserResource
	json.Unmarshal(resp.Body(), &keycloakUsers)
	proskiveUsers := keycloak.MultipleUsersToProSkive(keycloakUsers)

	// Write response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(proskiveUsers)
}

func (k *KeycloakController) GetUser(w http.ResponseWriter, r *http.Request) {
	// Get user id
	userId := chi.URLParam(r, "userId")

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/users/{userId}")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}
	// Do the request
	resp, err := resty.R().
		SetAuthToken(token).
		SetPathParams(map[string]string{
			"userId": userId,
		}).
		Get(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// If not found
	if resp.StatusCode() == http.StatusNotFound {
		ErrorResponse(w, r, http.StatusNotFound, "User was not found")
		return
	}

	// Convert response
	var keycloakUser keycloak.UserResource
	json.Unmarshal(resp.Body(), &keycloakUser)
	proskiveUser := keycloak.UserToProSkive(&keycloakUser)

	// Write response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(proskiveUser)
}

func (k *KeycloakController) AddUser(w http.ResponseWriter, r *http.Request) {
	// Get Body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ErrorResponse(w, r, http.StatusInternalServerError, "Could not parse body")
		return
	}

	// Unmarshal Body
	newUser := &proskive.UserResource{}
	err = json.Unmarshal(body, newUser)
	if err != nil {
		ErrorResponse(w, r, http.StatusBadRequest, "Your json contains some errors")
		return
	}

	// Map to Keycloak
	newKeycloakUser := keycloak.UserToKeycloak(newUser, "")

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/users")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do request
	resp, err := resty.R().
		SetAuthToken(token).
		SetBody(newKeycloakUser).
		Post(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Response
	w.WriteHeader(resp.StatusCode())
	json.NewEncoder(w).Encode(resp.Body())
}

func (k *KeycloakController) UpdateUser(w http.ResponseWriter, r *http.Request) {
	// User id
	userId := chi.URLParam(r, "userId")

	// Get Body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ErrorResponse(w, r, http.StatusInternalServerError, "Could not parse body")
		return
	}

	// Unmarshal Body
	updateUser := &proskive.UserResource{}
	err = json.Unmarshal(body, updateUser)
	if err != nil {
		ErrorResponse(w, r, http.StatusBadRequest, "Your json contains some errors")
		return
	}

	// Check Ids
	if updateUser.ID != userId {
		ErrorResponse(w, r, http.StatusBadRequest, "ID does not match to resource")
		return
	}

	// Map to Keycloak
	updateKeycloakUser := keycloak.UserToKeycloak(updateUser, userId)

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/users/{userId}")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do request
	resp, err := resty.R().
		SetAuthToken(token).
		SetBody(updateKeycloakUser).
		SetPathParams(map[string]string{
			"userId": userId,
		}).
		Put(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Response
	w.WriteHeader(resp.StatusCode())
	json.NewEncoder(w).Encode(resp.Body())
}

func (k *KeycloakController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	// User id
	userId := chi.URLParam(r, "userId")

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/users/{userId}")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do request
	resp, err := resty.R().
		SetAuthToken(token).
		SetPathParams(map[string]string{
			"userId": userId,
		}).
		Delete(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Response
	w.WriteHeader(resp.StatusCode())
	json.NewEncoder(w).Encode(resp.Body())
}

func (k *KeycloakController) GetRolesFromUser(w http.ResponseWriter, r *http.Request) {
	// User id
	userId := chi.URLParam(r, "userId")

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/users/{userId}/role-mappings/realm/composite")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do request
	resp, err := resty.R().
		SetAuthToken(token).
		SetPathParams(map[string]string{
			"userId": userId,
		}).
		Get(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Get Body
	var keycloakRoles []keycloak.RoleResource
	json.Unmarshal(resp.Body(), &keycloakRoles)
	proskiveRoles := keycloak.MultipleRolesToProSkive(keycloakRoles)

	// Response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.StatusCode())
	json.NewEncoder(w).Encode(proskiveRoles)
}

func (k *KeycloakController) AddRoleToUser(w http.ResponseWriter, r *http.Request) {
	// User id
	userId := chi.URLParam(r, "userId")

	// Role name
	roleName := chi.URLParam(r, "roleName")

	// Get role
	role, err := RoleByName(roleName)
	if err != nil {
		log.Error().Err(err).Msgf("Could not add role '%v' to user '%v'", roleName, userId)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Put role in a list
	roles := []keycloak.RoleResource{ *role }

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}


	// Compose endpoint
	url, err := keycloak.RestUrl("/users/{userId}/role-mappings/realm")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do request
	resp, err := resty.R().
		SetAuthToken(token).
		SetBody(roles).
		SetPathParams(map[string]string{
			"userId": userId,
		}).
		Post(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Respond
	w.WriteHeader(resp.StatusCode())
	json.NewEncoder(w).Encode(resp.Body())
}