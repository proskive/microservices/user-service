package controller

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/proskive/microservices/user-service/keycloak"
	"gopkg.in/resty.v1"
	"net/http"
)

func (k *KeycloakController) GetAllGroups(w http.ResponseWriter, r *http.Request) {
	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/groups")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do the request
	resp, err := resty.R().
		SetAuthToken(token).
		Get(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Convert response
	var keycloakGroups []keycloak.GroupResource
	json.Unmarshal(resp.Body(), &keycloakGroups)
	proskiveGroups := keycloak.MultipleGroupsToProSkive(keycloakGroups)

	// Write response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(proskiveGroups)
}