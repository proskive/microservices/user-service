package controller

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

type ErrorResource struct {
	Code    int       `json:"code"`
	Message string    `json:"message"`
	Path    string    `json:"path"`
	Time    time.Time `json:"time"`
}

func ErrorResponse(w http.ResponseWriter, r *http.Request, code int, message string) {
	if code >= 500 {
		log.Error().Str("Request URI", r.RequestURI).Msg(message)
	}

	resp := &ErrorResource{
		Code:    code,
		Message: message,
		Path:    r.RequestURI,
		Time:    time.Now(),
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(resp)
}
