package proskive

type UserResource struct {
	ID             string `json:"id"`
	Username       string `json:"username"`
	Title          string `json:"title"`
	FirstName      string `json:"firstName"`
	LastName       string `json:"lastName"`
	Clinic         string `json:"clinic"`
	Institute      string `json:"institute"`
	Street         string `json:"street"`
	Building       string `json:"building"`
	Room           string `json:"room"`
	Email          string `json:"email"`
	Phone          string `json:"phone"`
	Phone2         string `json:"phone2"`
	Mobile         string `json:"mobile"`
	Fax            string `json:"fax"`
	PolicyAccepted bool   `json:"policyAccepted"`
	Enabled        bool   `json:"enabled"`
	EmailVerified  bool   `json:"emailVerified"`
	Locale         string `json:"locale"`
}
