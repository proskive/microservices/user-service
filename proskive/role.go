package proskive

type RoleResource struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
