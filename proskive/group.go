package proskive

type GroupResource struct {
	ID        string          `json:"id"`
	Name      string          `json:"name"`
	Path      string          `json:"path"`
	Roles     []string        `json:"roles"`
	SubGroups []GroupResource `json:"subGroups"`
}
