# User Service

The user service is a highly performant layer between the auth system and the backend which can easily use different auth systems (currently [keycloak](https://www.keycloak.org/) only)

## Setting up Keycloak

When using `adapter: keycloak` in config.yaml or `$ADAPTER=keycloak` as env var.

#### Add Client

The User Service needs a sepcial configuration to work with Keycloak. Follow those steps:
 1. Add a Client (e.g. name="user-service")
 1. Set client type to `confidential`
 1. Enable option `Service Accounts Enabled`
 1. Go to the tab `Service Account Roles`
 1. Add the role: `Client Roles --> realm-management --> realm-admin`

#### Setup User Service

Additional steps are needed for the User Service to work:

 1. In Keycloak, head to the tab `Credentials` in client options
 1. Copy the `Secret` value (e.g.: 1111-1111-1111-1111)
 1. Now set the settings
    1. `$KEYCLOAK_URL=http://mykeycloak.domain.local` or yaml
    1. `$KEYCLOAK_REALM=MyRealm` or yaml
    1. `$KEYCLOAK_CLIENT=user-service` or yaml
    1. `$KEYCLOAK_SECRET=1111-1111-1111-1111` or yaml